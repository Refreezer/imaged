﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageD
{
    public partial class Form1 : Form
    {
        private Color picBoxDefaultColor;
        public Form1()
        {
            InitializeComponent();
            pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            saveButton.Enabled = false;
            picBoxDefaultColor = pictureBox.BackColor;
            pictureBox.AllowDrop = true;
        }

        private void OpenButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pictureBox.Image = new Bitmap(ofd.FileName);
                    saveButton.Enabled = true;
                }
                catch
                {
                    MessageBox.Show("Not an image", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                var destPath = folderBrowserDialog.SelectedPath;
                int height = (int) heightGrid.Value;
                int width = (int) widthGrid.Value;
                IEnumerable<Image> images = SplitImage(height, width);
                SaveImages(images, destPath);
            }
            else
            {
                MessageBox.Show("Something wrong with save", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region Logic

        private IEnumerable<Image> SplitImage(int gridHeight, int gridWidth)
        {
            Image image = pictureBox.Image;
            IList<Image> images = new List<Image>();


            int partWidth = image.Width / gridWidth;
            int partHeight = image.Height / gridHeight;

            Bitmap bitmap = image as Bitmap;
            for (int i = 0; i < image.Width; i += partWidth)
            {
                for (int j = 0; j < image.Height; j += partHeight)
                {
                    Bitmap partBmp = bitmap.Clone(
                        new Rectangle(i, j, partWidth, partHeight),
                        bitmap.PixelFormat
                    );
                    images.Add(partBmp);
                }
            }

            return images;
        }

        private void SaveImages(IEnumerable<Image> images, in string path)
        {
            int cnt = 1;


            foreach (var image in images)
            {
                image.Save(path + "\\part" + (cnt++) + ".gif", ImageFormat.Gif);
            }
        }

        #endregion

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO -- вывод инструкции
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //TODO -- вывод информации о приложении
        }

        private void pictureBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e?.Data?.GetData(DataFormats.FileDrop) != null)
            {
                string path = (e.Data.GetData(DataFormats.FileDrop) as string[])[0];
                try
                {
                    pictureBox.Image = new Bitmap(path);
                    saveButton.Enabled = true;
                    pictureBox.BackColor = picBoxDefaultColor;
                }
                catch
                {
                    MessageBox.Show("Not an image", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void pictureBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
                pictureBox.BackColor = Color.DarkSlateBlue;
            }
        }

        private void pictureBox_DragLeave(object sender, EventArgs e)
        {
            pictureBox.BackColor = picBoxDefaultColor;
        }
    }
}